LANGUAGES := $(shell ruby -ryaml -e "data = YAML::load(open('_config.yml')); puts data['languages']")

all:
	./bin/i18n.sh
	./bin/contributors.py
	jekyll build --verbose --trace

clean:
	rm -rf _site
	for lang in $(LANGUAGES); do \
	    rm -f $$lang/*.md $$lang/contribute/*.md _docs/$$lang/*.md; \
	    rmdir $$lang/contribute $$lang _docs/$$lang; \
	done

lint:
	@find -type f -not -wholename '*/_site/*' -print0 | xargs -0r grep -rl "href=[\"']/" | while read X; do \
		echo "W: $$X is using URIs that are not using '{{ \"/foo\" | relative_url }}'"; \
	done
