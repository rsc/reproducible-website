---
layout: report
year: "2022"
month: "06"
title: "Reproducible Builds in June 2022"
draft: false
date: 2022-07-13 13:58:37
---

[![]({{ "/images/reports/2022-06/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the June 2022 report from the [Reproducible Builds](https://reproducible-builds.org) project**. In these reports, we outline the most important things that we have been up to over the past month. As a quick recap, whilst anyone may inspect the source code of free software for malicious flaws, almost all software is distributed to end users as pre-compiled binaries.

<br>

#### Save the date!

Despite several delays, we are pleased to announce dates for our in-person summit this year:

<center><big>November 1st 2022 — November 3rd 2022</big></center><br>

The event will happen in/around Venice (Italy), and we intend to pick a venue reachable via the train station and an international airport. However, the precise venue will depend on the number of attendees.

Please see the [announcement mail](https://lists.reproducible-builds.org/pipermail/rb-general/2022-July/002652.html) from Mattia Rizzolo, and do keep an eye on the mailing list for further announcements as it will hopefully include registration instructions.

<br>

#### News

[![]({{ "/images/reports/2022-06/rust.jpg#right" | relative_url }})](https://www.rust-lang.org/)

David Wheeler filed an issue against the [Rust programming language](https://www.rust-lang.org/) to report that builds are "[not reproducible because full path to the source code is in the panic and debug strings](https://github.com/rust-lang/rust/issues/97955)". Luckily, as one of the responses mentions: "the `--remap-path-prefix` solves this problem and has been used to great effect in build systems that rely on reproducibility (Bazel, Nix) to work at all" and that "there are efforts to teach cargo about it [here](https://github.com/rust-lang/rfcs/pull/3127)".

<br>

The Python Security team announced that:

> The `ctx` hosted project on PyPI was taken over via user account compromise and replaced with a malicious project which contained runtime code which collected the content of `os.environ.items()` when instantiating Ctx objects. The captured environment variables were sent as a base64 encoded query parameter to a Heroku application [...]

As [their announcement](https://www.theregister.com/2022/05/24/pypi_ctx_package_compromised/) later goes onto state, version-pinning using "[hash-checking mode](https://pip.pypa.io/en/stable/topics/secure-installs/#hash-checking-mode)" can prevent this attack, although this does depend on specific installations using this mode, rather than a prevention that can be applied systematically.

<br>

[![]({{ "/images/reports/2022-06/vanitasvitae.png#right" | relative_url }})](https://blog.jabberhead.tk/2022/06/20/reproducible-builds-telling-of-a-debugging-story/)

Developer *vanitasvitae* published an interesting and entertaining blog post detailing the [blow-by-blow steps of debugging a reproducibility issue](https://blog.jabberhead.tk/2022/06/20/reproducible-builds-telling-of-a-debugging-story/) in [PGPainless](https://gh.pgpainless.org/), a library which "aims to make using [OpenPGP](https://www.openpgp.org/) in Java projects as simple as possible".

Whilst their in-depth research into the internals of the `.jar` may have been unnecessary given that [*diffoscope*](https://diffoscope.org/) would have identified the, it must be said that there is something to be said with occasionally delving into seemingly "low-level" details, as well describing *any* debugging process. Indeed, as *vanitasvitae* writes:

> Yes, this would have spared me from 3h of debugging 😉 But I probably would also not have gone onto this little dive into the JAR/ZIP format, so in the end I’m not mad.

<br>

[Kees Cook](https://outflux.net/) published a [short and practical blog post](https://outflux.net/blog/archives/2022/06/24/finding-binary-differences/) detailing how he uses reproducibility properties to aid [work to replace one-element arrays in the Linux kernel](https://github.com/KSPP/linux/issues/79). Kees' approach is based on the principle that if a (small) proposed change is considered equivalent by the compiler, then the generated output will be identical... but only if no other arbitrary or unrelated changes are introduced. Kees mentions the "fantastic" [*diffoscope*](https://diffoscope.org/) tool, as well as various kernel-specific build options (eg. `KBUILD_BUILD_TIMESTAMP`) in order to "prepare my build with the 'known to disrupt code layout' options disabled".

<br>

[![]({{ "/images/reports/2022-06/gdrsecurite.jpg#right" | relative_url }})](https://twitter.com/gloupin/status/1539536491197698049)

[Stefano Zacchiroli](https://upsilon.cc/~zack/) gave a presentation at [*GDR Sécurité Informatique*](https://gdr-securite.irisa.fr/en/gdr-securite-informatique-en/) based in part on a paper co-written with Chris Lamb titled [*Increasing the Integrity of Software Supply Chains*](https://arxiv.org/abs/2104.06020). ([Tweet](https://twitter.com/gloupin/status/1539536491197698049))

<br>

#### Debian

[![]({{ "/images/reports/2022-06/debian.png#right" | relative_url }})](https://debian.org/)

In Debian in this month, 28 reviews of Debian packages were added, 35 were updated and 27 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Two issue types were added: [ `nondeterministic_checksum_generated_by_coq`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/9a8b2665) and [`nondetermistic_js_output_from_webpack`](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/bd35c0c8).

After Holger Levsen found hundreds of [packages in the *bookworm* distribution that lack `.buildinfo` files](https://tests.reproducible-builds.org/debian/bookworm/amd64/index_no_buildinfos.html), he uploaded 404 source packages to the archive (with no meaningful source changes). Currently *bookworm* now shows only 8 packages without `.buildinfo` files, and those 8 are fixed in *unstable* and should migrate shortly. By contrast, Debian *unstable* will always have packages without `.buildinfo` files, as this is how they come through the [NEW queue](https://wiki.debian.org/NewQueue). However, as these packages were not built on the official build servers (ie. they were uploaded by the maintainer) they will never migrate to Debian *testing*. In the future, therefore, *testing* should never have packages without `.buildinfo` files again.

Roland Clobus posted yet another in-depth [status report about his progress making the Debian Live images build reproducibly](https://lists.reproducible-builds.org/pipermail/rb-general/2022-June/002649.html) to our [mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/). In this update, Roland mentions that "all major desktops build reproducibly with *bullseye*, *bookworm* and *sid*" but also goes on to outline the progress made with automated testing of the generated images using [openQA](http://open.qa/).

<br>

#### GNU Guix

[![]({{ "/images/reports/2022-06/guix.png#right" | relative_url }})](https://www.gnu.org/software/guix/)

Vagrant Cascadian made a significant number of contributions to [GNU Guix](https://www.gnu.org/software/guix/):

* Submitted patches to fix reproducibility issues in [keyutils](https://issues.guix.gnu.org/55758) and [isl](https://issues.guix.gnu.org/55757) as well as reported two bugs affecting reproducibility testing [[...](https://issues.guix.gnu.org/55822)][[...](https://issues.guix.gnu.org/55809)].

* 23 specific fixes related to reproducibility.&nbsp;[[1](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=824f2afcf0ffa7d515b04e360fa49a6b0c4a7753)][[2](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=3691e37eb14c5871d213646154b98f5aaa5dd2f1)][[3](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=89ab76b8109d494d204f81b6d934ea19c8bb7004)][[4](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=96d47797dce361bd7f3b1ce860b1ec4851e2db5a)][[5](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=5850f1e99ac8296c47cb42b1f6b222e8fa669eed)][[6](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=964cdd57fa037fec4917ac76725b0a65d47483bc)][[7](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=8bac52e6f31c732ddec8747378c2fad643a9e964)][[8](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=a04a987450908a84fa5fde0caa25a6a50027c73c)][[9](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=0c6123f8aa6236dcce1320cd190865324f3a5f94)][[10](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=ae1d8d6a6f3eb3f705394061be5fcf0efa996870)][[11](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=ce6d8ca4430e7df27c77e116a677679f241551da)][[12](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=41cf573f0a4fefb0539174b46cfe65e9db128870)][[13](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=14783af536a110ef5018518d2af920355b7391f9)][[14](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=7f85e7ce4900a6c948458ecd6022db00bc6428e2)][[15](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=2f3dec3de76011b2bd7b19e50a1b12b8d6697a34)][[16](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=b483ef1a889ad3f3b523d6f09f810a4618fc753e)][[17](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=7795a5dd6bb16ee5ec938ba26b91f69459639189)][[18](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=e92508baebebb4306779e6f4e2dff8c838b389cd)][[19](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=32322d0926313d28276928d81188ee909e464eb0)][[20](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=f1ec2d676df9822c3b74acf236f96011784a8642)][[21](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=76668c4b1356fd0c9be4ae4297cc7ad2141c2060)][[22](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=aaed8bab4614502822adeb999bad4ee0502009d9)][[23](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=615239d5caa5f668894bfccbb35f5ffa6b756615)]

* [Proposed setting `FORCE_SOURCE_DATE=1` in the environment of all builds](https://lists.gnu.org/archive/html/guix-devel/2022-06/msg00171.html) in order to fix numerous timestamp issues in documentation generation tools.

* Identified reproducibility issues in the `maradns` package as it [appears to embed a random prime number](https://lists.gnu.org/archive/html/guix-devel/2022-06/msg00110.html). ([Patch](https://github.com/samboy/MaraDNS/discussions/101#discussioncomment-3006487))

* [Responded in a thread](https://lists.gnu.org/archive/html/guix-devel/2022-06/msg00180.html) to point out that GNU Guix already has the infrastructure in place to verify the reproducibility of downloaded substitutes for the vast majority of packages.

* Lastly, Vagrant [performed an evaluation of the unreproducible packages](https://lists.gnu.org/archive/html/guix-devel/2022-06/msg00191.html) that remain in the distribution.

Elsewhere in GNU Guix, [Ludovic Courtès](https://people.bordeaux.inria.fr/lcourtes/) published a paper in the journal [*The Art, Science, and Engineering of Programming*](https://programming-journal.org/) called [*Building a Secure Software Supply Chain with GNU Guix*](https://programming-journal.org/2023/7/1/):

> This paper focuses on one research question: how can [Guix]((https://www.gnu.org/software/guix/) and similar systems allow users to securely update their software? [...] Our main contribution is a model and tool to authenticate new Git revisions. We further show how, building on Git semantics, we build protections against downgrade attacks and related threats. We explain implementation choices. This work has been deployed in production two years ago, giving us insight on its actual use at scale every day. The Git checkout authentication at its core is applicable beyond the specific use case of Guix, and we think it could benefit to developer teams that use Git.

A [full PDF of the text](https://arxiv.org/pdf/2206.14606v1) is available.

<br>

#### openSUSE

[![]({{ "/images/reports/2022-06/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In the world of [openSUSE](https://www.opensuse.org/), SUSE announced at [SUSECon](https://susecon.com/) that [they are preparing to meet SLSA level 4](https://documentation.suse.com/sbp/server-linux/html/SBP-SLSA4/index.html). ([SLSA](https://slsa.dev/) (**S**upply chain **L**evels for **S**oftware **A**rtifacts) is a new industry-led standardisation effort that aims to protect the integrity of the software supply chain.)

However, at the time of writing, timestamps within RPM archives are not normalised, so bit-for-bit identical reproducible builds are not possible. Some [*in-toto* provenance files published for SUSE's SLE-15-SP4](https://download.opensuse.org/update/leap/15.4/sle/noarch/) as one result of the SLSA level 4 effort. Old binaries are not rebuilt, so only new builds (e.g. maintenance updates) have this metadata added.

Lastly, Bernhard M. Wiedemann posted his [usual monthly openSUSE reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/26GFJIIFFJYI7RDR37CEC7OKLGCGNNUS/).

<br>

#### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-06/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions [`215`](https://diffoscope.org/news/diffoscope-215-released/), [`216`](https://diffoscope.org/news/diffoscope-216-released/) and [`217`](https://diffoscope.org/news/diffoscope-217-released/) to Debian *unstable*. Chris Lamb also made the following changes:

* New features:

    * Print profile output if we were called with `--profile` and we were killed via a `TERM` signal. This should help in situations where *diffoscope* is terminated due to some sort of timeout.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0fc439ee)]
    * Support both [PyPDF](https://pybrary.net/pyPdf/) 1.x and 2.x.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3adcd3bd)]

* Bug fixes:

    * Also catch `IndexError` exceptions (in addition to `ValueError`) when parsing `.pyc` files. ([#1012258](https://bugs.debian.org/1012258))
    * Correct the logic for supporting different versions of the `argcomplete` module.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/90c51e85)]

* Output improvements:

    * Don't leak the (likely-temporary) pathname when comparing PDF documents.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7252c305)]

* Logging improvements:

    * Update test fixtures for GNU [*readelf*](https://sourceware.org/binutils/docs/binutils/readelf.html) 2.38 (now in Debian *unstable*).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/eb9c46e5)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a4d45412)]
    * Be more specific about the minimum required version of `readelf` (ie. *binutils*), as it appears that this 'patch' level version change resulted in a change of output, not the 'minor' version.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/280ff401)]
    * Use our `@skip_unless_tool_is_at_least` decorator (NB. `at_least`) over `@skip_if_tool_version_is` (NB. `is`) to fix tests under Debian *stable*.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/8c3e3eef)]
    * Emit a warning if/when we are handling a UNIX `TERM` signal.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d77ff3a3)]

* Codebase improvements:

    * Clarify in what situations the main `finally` block gets called with respect to `TERM` signal handling.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c26ac469)]
    * Clarify control flow in the `diffoscope.profiling` module.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3e7491bd)]
    * Correctly package the `scripts/` directory.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/da5458ee)]

In addition, Edward Betts updated a broken link to the RSS on the [*diffoscope* homepage](https://diffoscope.org/) and Vagrant Cascadian updated the *diffoscope* package in [GNU Guix](https://www.gnu.org/software/guix/)&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=1d7222590361ecb0ff56b42872ca6e5754732d08)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=2900031f42ba85bebeffdb67a56b0b9cf92019e7)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=271736117e3f09b616a2dbd5d74c9595926c9297)].

<br>

#### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`build-compare`](https://github.com/openSUSE/build-compare/issues/52) caused [a regression](https://github.com/openSUSE/build-compare/issues/53) for a few days.
    * [`python-fasttext`](https://build.opensuse.org/request/show/980329) (CPU-related issue).

* Chris Lamb:

    * [#1012614](https://bugs.debian.org/1012614) filed against [`node-dommatrix`](https://tracker.debian.org/pkg/node-dommatrix).
    * [#1012766](https://bugs.debian.org/1012766) filed against [`rtpengine`](https://tracker.debian.org/pkg/rtpengine).
    * [#1012790](https://bugs.debian.org/1012790) filed against [`sphinxcontrib-mermaid`](https://tracker.debian.org/pkg/sphinxcontrib-mermaid).
    * [#1012792](https://bugs.debian.org/1012792) filed against [`yaru-theme`](https://tracker.debian.org/pkg/yaru-theme).
    * [#1012836](https://bugs.debian.org/1012836) filed against [`mapproxy`](https://tracker.debian.org/pkg/mapproxy) ([forwarded upstream](https://github.com/mapproxy/mapproxy/pull/585)).
    * [#1013257](https://bugs.debian.org/1013257) filed against [`libxsmm`](https://tracker.debian.org/pkg/libxsmm).
    * [#1014041](https://bugs.debian.org/1014041) filed against [`yt-dlp`](https://tracker.debian.org/pkg/yt-dlp) ([forwarded upstream](https://github.com/yt-dlp/yt-dlp/pull/4220)).
    * [#891263](https://bugs.debian.org/891263) was filed against [puppet](https://tracker.debian.org/pkg/puppet) in February 2018 and the patch was finally [proposed for inclusion upstream](https://github.com/puppetlabs/puppet/pull/8916).

<br>

#### Testing framework

[![]({{ "/images/reports/2022-06/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a significant testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, the following changes were made:

* Holger Levsen:

    * Add a package set for packages that use the [R programming language](https://www.r-project.org/)&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c00b2ebf)] as well as one for [Rust](https://rust-lang.org)&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1c2dc621)].
    * Improve package set matching for Python&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e7810c4f)] and font-related&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2922dcc9)] packages.
    * Install the `lz4`, `lzop` and `xz-utils` packages on all nodes in order to detect running kernels.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a3fc7cbb)]
    * Improve the cleanup mechanisms when testing the reproducibility of Debian Live images.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e71e3cf1)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f53a53bf)]
    * In the automated node health checks, deprioritise the "generic kernel warning".&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b3fd8eb7)]

* Roland Clobus ([Debian Live](https://www.debian.org/devel/debian-live/) image reproducibility):

    * Add various maintenance jobs to the Jenkins view.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9410c3b4)]
    * Cleanup old workspaces after 24 hours.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/3053ad84)]
    * Cleanup temporary workspace and resulting directories.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6e7fab1b)]
    * Implement a number of fixes and improvements around publishing files.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9d732de6)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/35a21dc4)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/62df66d4)]
    * Don't attempt to preserve the file timestamps when copying artifacts.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8dcc081a)]

And finally, node maintenance was also performed by Mattia Rizzolo&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6015fac0)].

<br>

#### Mailing list and website

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* David Wheeler [started a thread stating his desire that reproducible builds and GitBOM are able to work together simultaneously](https://lists.reproducible-builds.org/pipermail/rb-general/2022-June/002637.html). David first describes the goals of both [GitBOM](https://gitbom.dev/) and reproducibility, outlines the potential problems and even outlines a number of prospective solutions.

* In a similar vein, David Wheeler also posted about the [problems with Profile-Guided Optimisation (PGO) in relation to reproducible builds](https://lists.reproducible-builds.org/pipermail/rb-general/2022-June/002633.html).

* Roland Clobus "copied in" our mailing list with a question about whether [enabling link-time optimisations (LTO) in Debian as a whole might cause reproducibility problems](https://lists.reproducible-builds.org/pipermail/rb-general/2022-June/002631.html).

* Mattia Rizzolo posted a [request for assistance regarding the translations of our website](https://lists.reproducible-builds.org/pipermail/rb-general/2022-June/002612.html).

Lastly, Chris Lamb updated the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) in a number of small ways, but primarily [published an interview]({{ "/news/2022/06/24/supporter-spotlight-hans-christoph-steiner-f-droid-project/" | relative_url }}) with [Hans-Christoph Steiner](https://at.or.at/) of the [F-Droid](https://f-droid.org/) project. Chris Lamb also added a [Coffeescript](https://coffeescript.org/) example for parsing and using the `SOURCE_DATE_EPOCH` environment variable&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/a81d0273)]. In addition, Sebastian Crane very-helpfully updated the screenshot of *salsa.debian.org*'s "request access" button on the [*How to join the Salsa group*]({{ "/contribute/salsa/" | relative_url }}).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d34c7ea3)]

<br>

#### Contact

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
